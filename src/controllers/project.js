const knex = require("../database/DatabaseConfig");
const { v4: uuid } = require("uuid");

module.exports = {
    async addOneProject(req, res) {
        if (!req.body.name) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Invalid data",
                },
            });
        } else {
            const projectName = req.body.name;
            try {
                await knex("project")
                    .where({
                        name: projectName,
                    })
                    .then(async (result) => {
                        if (result.length) {
                            return res.status(200).json({
                                status: {
                                    is_error: true,
                                    timestamp: new Date(),
                                    message: "Project already registered",
                                },
                            });
                        } else {
                            const projectId = uuid();
                            await knex("project")
                                .insert({
                                    id: projectId,
                                    name: projectName,
                                })
                                .then(() => {
                                    return res.status(200).json({
                                        status: {
                                            is_error: false,
                                            timestamp: new Date(),
                                            message: "Project registered",
                                        },
                                        data: {
                                            project: {
                                                id: projectId,
                                                name: projectName,
                                                created_at: new Date(),
                                                updated_at: null,
                                            },
                                        },
                                    });
                                });
                        }
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
    async updateProject(req, res) {
        if (!req.body.newName || !req.params.id) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Invalid data",
                },
            });
        } else {
            const newName = req.body.newName;
            const projectId = req.params.id;
            try {
                await knex("project")
                    .where({ id: projectId })
                    .then(async (result) => {
                        if (!result.length) {
                            return res.status(200).json({
                                status: {
                                    is_error: true,
                                    timestamp: new Date(),
                                    message: "Project not found",
                                },
                            });
                        } else {
                            await knex("project")
                                .where({ id: projectId })
                                .update({
                                    name: newName,
                                    updated_at: new Date(),
                                })
                                .then(() => {
                                    return res.status(200).json({
                                        status: {
                                            is_error: false,
                                            timestamp: new Date(),
                                            message: "Project Updated",
                                        },
                                        data: {
                                            project: {
                                                name: newName,
                                                updated_at: new Date(),
                                            },
                                        },
                                    });
                                });
                        }
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
    async getAllprojects(req, res) {
        try {
            await knex("project")
                .select("id", "name", "created_at", "updated_at")
                .where({ deleted: false })
                .then((result) => {
                    return res.status(200).json({
                        status: {
                            is_error: false,
                            timestamp: new Date(),
                            message: "Success",
                        },
                        data: {
                            projects: result,
                        },
                    });
                });
        } catch (error) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: error.message,
                },
            });
        }
    },
    async deleteOneProject(req, res) {
        if (!req.params.id) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Necessary id for this",
                },
            });
        } else {
            const id = req.params.id;
            try {
                await knex("project")
                    .where({ id: id })
                    .then(async (result) => {
                        if (!result.length) {
                            return res.status(200).json({
                                status: {
                                    is_error: true,
                                    timestamp: new Date(),
                                    message: "Project not found",
                                },
                            });
                        } else {
                            await knex("project")
                                .where({ id: id })
                                .update({ deleted: true })
                                .then(() => {
                                    return res.status(200).json({
                                        status: {
                                            is_error: false,
                                            timestamp: new Date(),
                                            message: "Project deleted",
                                        },
                                    });
                                });
                        }
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
};

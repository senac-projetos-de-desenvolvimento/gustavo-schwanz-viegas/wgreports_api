const knex = require("../database/DatabaseConfig");
const { v4: uuid } = require("uuid");
const bcrypt = require("bcrypt");

module.exports = {
    async addNewUser(req, res) {
        if (
            !req.body.name ||
            !req.body.password ||
            !req.body.email ||
            !req.body.authorization_id
        ) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Invalid data",
                },
            });
        } else {
            const { name, email, password, authorization_id } = req.body;

            try {
                const result = await knex("user").where({ email: email });

                if (result.length) {
                    return res.status(200).json({
                        status: {
                            is_error: true,
                            timestamp: new Date(),
                            message: "User already registered",
                        },
                    });
                } else {
                    const saltRounds = 10;
                    const encryptedPassword = bcrypt.hashSync(
                        password,
                        saltRounds
                    );

                    const userId = uuid();

                    await knex("user")
                        .insert({
                            id: userId,
                            name: name,
                            email: email,
                            password: encryptedPassword,
                            authorization_id: authorization_id,
                        })
                        .then(() => {
                            return res.status(200).json({
                                status: {
                                    is_error: false,
                                    timestamp: new Date(),
                                    message: "User registered",
                                },
                                data: {
                                    user: {
                                        id: userId,
                                        name: name,
                                        email: email,
                                        created_at: new Date(),
                                    },
                                },
                            });
                        });
                }
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
    async getUserById(req, res) {
        if (!req.params.id) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Necessary id for this",
                },
            });
        } else {
            const id = req.params.id;
            try {
                await knex
                    .select(
                        "u.id",
                        "u.name",
                        "u.email",
                        "u.created_at",
                        "u.updated_at",
                        "a.name as authorization_name",
                        "a.level as authorization_level"
                    )
                    .from("user as u")
                    .join("authorization as a", "u.authorization_id", "a.id")
                    .where({ deleted: false, 'u.id': id })
                    .then((result) => {
                        return res.status(200).json({
                            status: {
                                is_error: false,
                                timestamp: new Date(),
                                message: "Success",
                            },
                            data: {
                                user: result[0],
                            },
                        });
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
    async getAllUsers(req, res) {
        try {
            await knex
                .select(
                    "u.id",
                    "u.name",
                    "u.email",
                    "u.created_at",
                    "u.updated_at",
                    "a.name as authorization_name",
                    "a.level as authorization_level"
                )
                .from("user as u")
                .join("authorization as a", "u.authorization_id", "a.id")
                .where({ deleted: false })
                .then((result) => {
                    return res.status(200).json({
                        status: {
                            is_error: false,
                            timestamp: new Date(),
                            message: "Success",
                        },
                        data: {
                            users: result.map((user) => {
                                return {
                                    id: user.id,
                                    name: user.name,
                                    email: user.email,
                                    createdAt: user.created_at,
                                    updatedAt: user.updated_at,
                                    authorization: {
                                        name: user.authorization_name,
                                        level: user.authorization_level,
                                    },
                                };
                            }),
                        },
                    });
                });
        } catch (error) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: error.message,
                },
            });
        }
    },
    async deleteUser(req, res) {
        if (!req.params.id) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Necessary id for this",
                },
            });
        } else {
            const id = req.params.id;
            try {
                await knex("user")
                    .where({ id: id, deleted: false })
                    .then(async (result) => {
                        if (!result.length) {
                            return res.status(200).json({
                                status: {
                                    is_error: true,
                                    timestamp: new Date(),
                                    message: "User not found",
                                },
                            });
                        } else {
                            await knex("user")
                                .where({ id: id })
                                .update({ deleted: true })
                                .then(() => {
                                    return res.status(200).json({
                                        status: {
                                            is_error: false,
                                            timestamp: new Date(),
                                            message: "User deleted",
                                        },
                                    });
                                });
                        }
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
    async updateUser(req, res) {
        if (!req.params.id) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Necessary id for this",
                },
            });
        } else {
            const id = req.params.id;
            const { newPassword } = req.body;
            try {
                await knex("user")
                    .where({ id: id, deleted: false })
                    .then(async (result) => {
                        if (!result.length) {
                            return res.status(200).json({
                                status: {
                                    is_error: true,
                                    timestamp: new Date(),
                                    message: "User not found",
                                },
                            });
                        } else {
                            const saltRounds = 10;
                            const encryptedNewPassword = bcrypt.hashSync(
                                newPassword,
                                saltRounds
                            );
                            await knex("user")
                                .where({ id: id })
                                .update({
                                    password: encryptedNewPassword,
                                    updated_at: new Date(),
                                })
                                .then(() => {
                                    return res.status(200).json({
                                        status: {
                                            is_error: false,
                                            timestamp: new Date(),
                                            message: "User updated",
                                        },
                                    });
                                });
                        }
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
};

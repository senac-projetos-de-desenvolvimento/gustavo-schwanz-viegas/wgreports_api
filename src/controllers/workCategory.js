const knex = require("../database/DatabaseConfig");
const { v4: uuid } = require("uuid");

module.exports = {
    async addOneCategory(req, res) {
        if (!req.body.name) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Invalid data",
                },
            });
        } else {
            const workCategoryName = req.body.name;
            try {
                await knex("work_category")
                    .where({
                        name: workCategoryName,
                    })
                    .then(async (result) => {
                        if (result.length) {
                            return res.status(200).json({
                                status: {
                                    is_error: true,
                                    timestamp: new Date(),
                                    message: "Work category already registered",
                                },
                            });
                        } else {
                            const categoryId = uuid();
                            await knex("work_category")
                                .insert({
                                    id: categoryId,
                                    name: workCategoryName,
                                })
                                .then(() => {
                                    return res.status(200).json({
                                        status: {
                                            is_error: false,
                                            timestamp: new Date(),
                                            message: "Work category registered",
                                        },
                                        data: {
                                            workCategory: {
                                                id: categoryId,
                                                name: workCategoryName,
                                                created_at: new Date(),
                                                updated_at: null,
                                            },
                                        },
                                    });
                                });
                        }
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
    async updateCategory(req, res) {
        if (!req.body.newName || !req.params.id) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Invalid data",
                },
            });
        } else {
            const newName = req.body.newName;
            const categoryId = req.params.id;
            try {
                await knex("work_category")
                    .where({ id: categoryId })
                    .then(async (result) => {
                        if (!result.length) {
                            return res.status(200).json({
                                status: {
                                    is_error: true,
                                    timestamp: new Date(),
                                    message: "Work category not found",
                                },
                            });
                        } else {
                            await knex("work_category")
                                .where({ id: categoryId })
                                .update({
                                    name: newName,
                                    updated_at: new Date(),
                                })
                                .then(() => {
                                    return res.status(200).json({
                                        status: {
                                            is_error: false,
                                            timestamp: new Date(),
                                            message: "Work category Updated",
                                        },
                                        data: {
                                            workCategory: {
                                                name: newName,
                                                updated_at: new Date(),
                                            },
                                        },
                                    });
                                });
                        }
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
    async getAllCategories(req, res) {
        try {
            await knex("work_category")
                .select("id", "name", "created_at", "updated_at")
                .where({ deleted: false })
                .then((result) => {
                    return res.status(200).json({
                        status: {
                            is_error: false,
                            timestamp: new Date(),
                            message: "Success",
                        },
                        data: {
                            workCategories: result,
                        },
                    });
                });
        } catch (error) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: error.message,
                },
            });
        }
    },
    async deleteOneCategory(req, res) {
        if (!req.params.id) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Necessary id for this",
                },
            });
        } else {
            const id = req.params.id;
            try {
                await knex("work_category")
                    .where({ id: id })
                    .then(async (result) => {
                        if (!result.length) {
                            return res.status(200).json({
                                status: {
                                    is_error: true,
                                    timestamp: new Date(),
                                    message: "Work category not found",
                                },
                            });
                        } else {
                            await knex("work_category")
                                .where({ id: id })
                                .update({ deleted: true })
                                .then(() => {
                                    return res.status(200).json({
                                        status: {
                                            is_error: false,
                                            timestamp: new Date(),
                                            message: "Work category deleted",
                                        },
                                    });
                                });
                        }
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
};

const knex = require("../database/DatabaseConfig");
const { v4: uuid } = require("uuid");

module.exports = {
    async addNewAuthorization(req, res) {
        if (!req.body.name || !req.body.level) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Invalid data",
                },
            });
        }
        const { name, level } = req.body;
        const authorizationId = uuid();
        try {
            const result = await knex("authorization").where({ name: name });
            if (result.length) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: "Authorization already registered",
                    },
                });
            } else {
                await knex("authorization")
                    .insert({
                        id: authorizationId,
                        name: name,
                        level: level,
                    })
                    .then(() => {
                        return res.status(200).json({
                            status: {
                                is_error: false,
                                timestamp: new Date(),
                                message: "Authorization registered",
                            },
                            data: {
                                authorization: {
                                    id: authorizationId,
                                    name: name,
                                },
                            },
                        });
                    });
            }
        } catch (error) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: error.message,
                },
            });
        }
    },
    async getAllAuthorizaions(req, res) {
        try {
            await knex("authorization")
                .select("authorization.name", "authorization.id")
                .then((result) => {
                    return res.status(200).json({
                        status: {
                            is_error: false,
                            timestamp: new Date(),
                            message: "Success",
                        },
                        data: {
                            authorizations: result,
                        },
                    });
                });
        } catch (error) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: error.message,
                },
            });
        }
    },
};

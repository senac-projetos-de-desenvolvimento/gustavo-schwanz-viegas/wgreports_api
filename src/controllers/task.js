const knex = require("../database/DatabaseConfig");
const { v4: uuid } = require("uuid");

module.exports = {
    async getAllTasks(req, res) {
        try {
            await knex("task")
                .where({ deleted: false })
                .then((result) => {
                    return res.status(200).json({
                        status: {
                            is_error: false,
                            timestamp: new Date(),
                            message: "Success",
                        },
                        data: {
                            tasks: result,
                        },
                    });
                });
        } catch (error) {
            console.log(error);
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: error.message,
                },
            });
        }
    },
    async addTask(req, res) {
        const body = req.body;
        if (
            !body.description ||
            !body.expected_time ||
            !body.work_category_id ||
            !body.project_id
        ) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Invalid data",
                },
            });
        } else {
            const {
                description,
                expected_time,
                work_category_id,
                project_id,
                user_id,
            } = body;
            const taskId = uuid();
            const userId = req.headers["user-id"];
            try {
                await knex("task")
                    .insert({
                        id: taskId,
                        description: description,
                        expected_time: expected_time,
                        created_at: new Date(),
                        work_category_id: work_category_id,
                        project_id: project_id,
                        user_id: userId,
                    })
                    .then(() => {
                        return res.status(200).json({
                            status: {
                                is_error: false,
                                timestamp: new Date(),
                                message: "Task registered",
                            },
                            data: {
                                task: {
                                    id: taskId,
                                    description: description,
                                    expected_time: expected_time,
                                    time_spent: undefined,
                                    created_at: new Date(),
                                    updated_at: undefined,
                                    completed: false,
                                    work_category_id: work_category_id,
                                    project_id: project_id,
                                    user_id: user_id,
                                },
                            },
                        });
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
    async removeTask(req, res) {
        if (!req.params.id) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Necessary id for this",
                },
            });
        } else {
            const id = req.params.id;
            try {
                await knex("task")
                    .where({ id: id })
                    .then(async (result) => {
                        if (!result.length) {
                            return res.status(200).json({
                                status: {
                                    is_error: true,
                                    timestamp: new Date(),
                                    message: "Task not found",
                                },
                            });
                        } else {
                            await knex("task")
                                .where({ id: id })
                                .update({
                                    deleted: true,
                                    updated_at: new Date(),
                                })
                                .then(() => {
                                    return res.status(200).json({
                                        status: {
                                            is_error: false,
                                            timestamp: new Date(),
                                            message: "Task deleted",
                                        },
                                    });
                                });
                        }
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
    async getUserTasks(req, res) {
        if (!req.params.id) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Necessary id for this",
                },
            });
        } else {
            const id = req.params.id;
            try {
                await knex("task")
                    .where({ user_id: id })
                    .then((result) => {
                        return res.status(200).json({
                            status: {
                                is_error: true,
                                timestamp: new Date(),
                                message: "Success",
                            },
                            data: {
                                tasks: result,
                            },
                        });
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
};

const knex = require("../database/DatabaseConfig");
const { v4: uuid } = require("uuid");

module.exports = {
    async getAllHindrances(req, res) {
        try {
            await knex("hindrance")
                .where({ deleted: false })
                .then((result) => {
                    return res.status(200).json({
                        status: {
                            is_error: false,
                            timestamp: new Date(),
                            message: "Success",
                        },
                        data: {
                            hindrances: result,
                        },
                    });
                });
        } catch (error) {
            console.log(error);
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: error.message,
                },
            });
        }
    },
    async addHindrance(req, res) {
        const body = req.body;
        if (
            !body.description ||
            !body.public ||
            !body.work_category_id ||
            !body.project_id
        ) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Invalid data",
                },
            });
        } else {
            const {
                description,
                public,
                work_category_id,
                project_id,
                user_id,
            } = body;
            const hidranceId = uuid();
            const userId = req.headers["user-id"];
            try {
                await knex("hindrance")
                    .insert({
                        id: hidranceId,
                        description: description,
                        public: public,
                        created_at: new Date(),
                        work_category_id: work_category_id,
                        project_id: project_id,
                        user_id: userId,
                    })
                    .then(() => {
                        return res.status(200).json({
                            status: {
                                is_error: false,
                                timestamp: new Date(),
                                message: "Hindrance registered",
                            },
                            data: {
                                hindrance: {
                                    id: hidranceId,
                                    description: description,
                                    public: public,
                                    resolved: false,
                                    created_at: new Date(),
                                    updated_at: undefined,
                                    work_category_id: work_category_id,
                                    project_id: project_id,
                                    user_id: user_id,
                                },
                            },
                        });
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
    async removeHindrance(req, res) {
        if (!req.params.id) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Necessary id for this",
                },
            });
        } else {
            const id = req.params.id;
            try {
                await knex("hindrance")
                    .where({ id: id })
                    .then(async (result) => {
                        if (!result.length) {
                            return res.status(200).json({
                                status: {
                                    is_error: true,
                                    timestamp: new Date(),
                                    message: "Hindrance not found",
                                },
                            });
                        } else {
                            await knex("hindrance")
                                .where({ id: id })
                                .update({
                                    deleted: true,
                                    updated_at: new Date(),
                                })
                                .then(() => {
                                    return res.status(200).json({
                                        status: {
                                            is_error: false,
                                            timestamp: new Date(),
                                            message: "Hindrance deleted",
                                        },
                                    });
                                });
                        }
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
    async resolveHindrance(req, res) {
        if (!req.params.id) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Necessary id for this",
                },
            });
        } else {
            const id = req.params.id;
            try {
                await knex("hindrance")
                    .where({ id: id })
                    .then(async (result) => {
                        if (!result.length) {
                            return res.status(200).json({
                                status: {
                                    is_error: true,
                                    timestamp: new Date(),
                                    message: "Hindrance not found",
                                },
                            });
                        } else {
                            await knex("hindrance")
                                .where({ id: id })
                                .update({
                                    resolved: true,
                                    updated_at: new Date(),
                                })
                                .then(() => {
                                    return res.status(200).json({
                                        status: {
                                            is_error: false,
                                            timestamp: new Date(),
                                            message: "Hindrance updated",
                                        },
                                    });
                                });
                        }
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
    async getUserHindrances(req, res) {
        if (!req.params.id) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Necessary id for this",
                },
            });
        } else {
            const id = req.params.id;
            try {
                await knex("hindrance")
                    .where({ user_id: id })
                    .then((result) => {
                        return res.status(200).json({
                            status: {
                                is_error: true,
                                timestamp: new Date(),
                                message: "Success",
                            },
                            data: {
                                hindrances: result,
                            },
                        });
                    });
            } catch (error) {
                return res.status(200).json({
                    status: {
                        is_error: true,
                        timestamp: new Date(),
                        message: error.message,
                    },
                });
            }
        }
    },
};

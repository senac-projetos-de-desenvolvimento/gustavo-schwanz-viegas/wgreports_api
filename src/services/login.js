const jwt = require("jsonwebtoken");
const knex = require("../database/DatabaseConfig");
const bcrypt = require("bcrypt");

module.exports = {
    async loginService(req, res) {
        if (!req.body.email || !req.body.password) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: "Invalid login data",
                },
            });
        }

        const { email, password } = req.body;

        try {
            await knex
                .select(
                    "u.id",
                    "u.name",
                    "u.password",
                    "u.email",
                    "u.created_at",
                    "u.updated_at",
                    "a.name as authorization_name",
                    "a.level as authorization_level"
                )
                .from("user as u")
                .join("authorization as a", "u.authorization_id", "a.id")
                .where({ email: email, deleted: false })
                .then((result) => {
                    if (!result.length) {
                        return res.status(200).json({
                            status: {
                                is_error: true,
                                timestamp: new Date(),
                                message: "User not found",
                            },
                        });
                    }
                    const user = { ...result[0] };
                    if (bcrypt.compareSync(password, user.password)) {
                        const token = jwt.sign(
                            {
                                email: user.email,
                                id: user.id,
                            },
                            process.env.JWT_KEY,
                            {
                                expiresIn: "4h",
                            }
                        );
                        return res.status(200).json({
                            status: {
                                is_error: false,
                                timestamp: new Date(),
                                message: "Success",
                            },
                            data: {
                                user: {
                                    id: user.id,
                                    name: user.name,
                                    email: user.email,
                                    createdAt: user.created_at,
                                    updatedAt: user.updated_at,
                                    authorization: {
                                        name: user.authorization_name,
                                        level: user.authorization_level,
                                    },
                                },
                                token: {
                                    bearer: token,
                                },
                            },
                        });
                    } else {
                        return res.status(200).json({
                            status: {
                                is_error: true,
                                timestamp: new Date(),
                                message: "Incorrect password",
                            },
                        });
                    }
                });
        } catch (error) {
            return res.status(200).json({
                status: {
                    is_error: true,
                    timestamp: new Date(),
                    message: error.message,
                },
            });
        }
    },
};

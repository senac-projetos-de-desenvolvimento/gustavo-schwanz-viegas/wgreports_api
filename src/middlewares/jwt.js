const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const parsedToken = jwt.verify(token, process.env.JWT_KEY);
        const userId = req.headers["user-id"];
        if (userId === parsedToken.id) {
            next();
        } else {
            throw new Error({ message: "Invalid user id" });
        }
    } catch (error) {
        return res.status(200).json({
            status: {
                is_error: true,
                timestamp: new Date(),
                message: error.message,
            },
        });
    }
};

create database wg_database;

use  wg_database;

create table authorization(
    id varchar(60) primary key not null,
    name varchar(45) not null UNIQUE,
    level int(3) not null,
    created_at timestamp not null default CURRENT_TIMESTAMP,
    updated_at timestamp
);

create table user(
    id varchar(60) primary key not null,
    email varchar(60) UNIQUE not null,
    password varchar(100) not null,
    name varchar(45) not null,
    deleted boolean default false,
    created_at timestamp not null default CURRENT_TIMESTAMP,
    updated_at timestamp,
    authorization_id varchar(60) not null,
    foreign key (authorization_id) references authorization(id)
);

create table project(
    id varchar(60) primary key not null,
    name varchar(45) not null,
    deleted boolean default false,
    created_at timestamp not null default CURRENT_TIMESTAMP,
    updated_at timestamp
);

create table work_category(
    id varchar(60) primary key not null,
    name varchar(45) not null,
    deleted boolean default false,
    created_at timestamp not null default CURRENT_TIMESTAMP,
    updated_at timestamp
);

create table task(
    id varchar(60) primary key not null,
    description varchar(600) not null,
    expected_time varchar(10),
    time_spent varchar(10),
    deleted boolean default false,
    completed boolean default false,
    created_at timestamp not null default CURRENT_TIMESTAMP,
    updated_at timestamp,
    work_category_id varchar(60) not null,
    project_id varchar(60) not null,
    user_id varchar(60) not null,
    foreign key (work_category_id) references work_category(id),
    foreign key (project_id) references project(id),
    foreign key (user_id) references user(id)
);

create table hindrance(
    id varchar(60) primary key not null,
    description varchar(600) not null,
    resolved boolean default false,
    deleted boolean default false,
    public boolean default false,
    created_at timestamp not null default CURRENT_TIMESTAMP,
    updated_at timestamp,
    user_id varchar(60) not null,
    work_category_id varchar(60) not null,
    project_id varchar(60) not null,
    foreign key (user_id) references user(id),
    foreign key (work_category_id) references work_category(id),
    foreign key (project_id) references project(id)
);

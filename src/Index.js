const express = require("express");
const routes = require("./Routes");

const app = express();

app.use(express.json());
app.use(routes);

app.listen(process.env.API_PORT, () => {
    console.log("Server started", new Date().toString());
});

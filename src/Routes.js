const express = require("express");
const cors = require("cors");

const routes = express.Router();
routes.use(cors());

const { loginService } = require("./services/login");

const {
    addNewUser,
    getAllUsers,
    deleteUser,
    updateUser,
    getUserById,
} = require("./controllers/user");

const {
    addNewAuthorization,
    getAllAuthorizaions,
} = require("./controllers/authorization");

const {
    addOneProject,
    deleteOneProject,
    getAllprojects,
    updateProject,
} = require("./controllers/project");

const {
    addOneCategory,
    deleteOneCategory,
    getAllCategories,
    updateCategory,
} = require("./controllers/workCategory");

const {
    addTask,
    getAllTasks,
    getUserTasks,
    removeTask,
} = require("./controllers/task");

const {
    addHindrance,
    removeHindrance,
    getAllHindrances,
    getUserHindrances,
    resolveHindrance,
} = require("./controllers/hindrance")

const jwt = require("./middlewares/jwt");

routes
    .post("/login", loginService)
    .post("/addNewUser", jwt, addNewUser)
    .get("/getUserById/:id", jwt, getUserById)
    .get("/getAllUsers", jwt, getAllUsers)
    .delete("/deleteUser/:id", jwt, deleteUser)
    .put("/updateUser/:id", jwt, updateUser);

routes
    .get("/getAllAuthorizaions", jwt, getAllAuthorizaions)
    .post("/addNewAuthorization", jwt, addNewAuthorization);

routes
    .get("/getAllprojects", jwt, getAllprojects)
    .post("/addOneProject", jwt, addOneProject)
    .delete("/deleteOneProject/:id", jwt, deleteOneProject)
    .put("/updateProject/:id", jwt, updateProject);

routes
    .get("/getAllCategories", jwt, getAllCategories)
    .post("/addOneCategory", jwt, addOneCategory)
    .delete("/deleteOneCategory/:id", jwt, deleteOneCategory)
    .put("/updateCategory/:id", jwt, updateCategory);

routes
    .get("/getAllTasks", jwt, getAllTasks)
    .get("/getUserTasks/:id", jwt, getUserTasks)
    .post("/addTask", jwt, addTask)
    .delete("/removeTask/:id", jwt, removeTask);

routes
    .get("/getAllHindrances", jwt, getAllHindrances)
    .get("/getUserHindrances/:id", jwt, getUserHindrances)
    .post("/addHindrance", jwt, addHindrance)
    .delete("/removeHindrance/:id", jwt, removeHindrance)
    .put("/resolveHindrance/:id", jwt, resolveHindrance)

module.exports = routes;
